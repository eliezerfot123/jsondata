import json
import psycopg2
import urllib2

""" connect to the database """
def connect():
    conn = psycopg2.connect(" \
        dbname=testJson \
        host= localhost \
        user=pyfot \
        password=pyfot")
    return conn


"""function that save the information"""
def save_data(data):
    """ Here we establish the connection by invoking the connect function,
    the varible (data) will come of the data of the json,
    After of execute, we can commit the change at the database
    close the connection.
    """
    conn = connect()
    cursor = conn.cursor()
    cursor.execute('INSERT INTO data VALUES %s', (data,))
    conn.commit()
    conn.close()



"""function for show data save. """
def load_data(id):
    conn = connect()
    cursor = conn.cursor()
    cursor.excute('SELECT * FROM data WHERE id = %s', (id,))
    information = cursor.fetchone()
    conn.close()
    return information

""" Now we load the url in the project with the libraty urllib2 """
datos = urllib2.urlopen('http://www.citibikenyc.com/stations/json')
data = json.load(datos)
""" application date json view """
executionTime = data["executionTime"]
print  executionTime
stationBeanList = data["stationBeanList"]
print "Wait place load..."
for station in stationBeanList:
    to_save =  (
            station['id'],
            executionTime,
            station['stationName'],
            station['availableDocks'],
            station['totalDocks'],
            station['latitude'],
            station['longitude'],
            station['statusValue'],
            station['statusKey'],
            station['availableBikes'],
            station['stAddress1'],
            station['stAddress2'],
            station['city'],
            station['postalCode'],
            station['location'],
            station['altitude'],
            station['testStation'],
            station['lastCommunicationTime'],
            station['landMark'])
    save_data(to_save)
print "Complete"

datos.close()
